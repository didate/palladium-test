package com.app;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.app.entity.Department;
import com.app.entity.Designation;
import com.app.repository.DepartmentRepository;
import com.app.repository.DesignationRepository;
import com.app.utils.StaticData;


@Component
@Order(1)
public class FilterRequest implements Filter {

	@Autowired
	DesignationRepository designationRepository;
	
	@Autowired
	DepartmentRepository departementRepository;
	
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {		
		
		HttpSession session = ((HttpServletRequest)request).getSession();
		
		//Load base data
		if(session.getAttribute("isInit")==null){			
		List<Designation> designations = designationRepository.findAll();			
		if(designations.size()==0){
			for (Designation designation : StaticData.DESIGNATION) {
				designationRepository.save(designation);
			}
		}
		
		
		List<Department> departments = departementRepository.findAll();			
		if(departments.size()==0){
			for (Department department : StaticData.DEPARTMENT) {
				departementRepository.save(department);
			}
		}
			session.setAttribute("isInit", 1);
			
		}
		
		
		filterChain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
