package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PraticalTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PraticalTestApplication.class, args);
	}
}
