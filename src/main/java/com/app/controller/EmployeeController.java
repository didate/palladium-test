package com.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.entity.Employee;
import com.app.repository.DepartmentRepository;
import com.app.repository.DesignationRepository;
import com.app.repository.EmployeeRepository;

/**
 * Controller class of employee
 * @author ml.diallo
 *
 */

@Controller
public class EmployeeController {	
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	DesignationRepository designationRepository;
	
	@Autowired
	DepartmentRepository departementRepository;
	
	/**
	 * Return the Employees List
	 * @param employeeNo
	 * @param model
	 * @param success
	 * @param error
	 * @return
	 */
	@RequestMapping(value="/", method= RequestMethod.GET)
	public String employees(@RequestParam(required=false,value="employeeNo") Long employeeNo, Model model, 
			@ModelAttribute("flash") String success,
			@ModelAttribute("error") String error){		
		
		model.addAttribute("employee", employeeNo!=null ? employeeRepository.findOne(employeeNo):new Employee() );
		model.addAttribute("employees", employeeRepository.findAll());
		model.addAttribute("departments", departementRepository.findAll());
		model.addAttribute("designations", designationRepository.findAll());
		model.addAttribute("success", success); //success message
		model.addAttribute("error", error); //error message
		return "/employee/list";
	}
	
	
	/**
	 * Add the New entry or Update an existing entry 
	 * @param employee
	 * @param rBindingResult
	 * @param model
	 * @param redirect
	 * @return 
	 */
	@PostMapping(value="/valid")
	public String validEntry(@Valid Employee employee, BindingResult rBindingResult, Model model, RedirectAttributes redirect){
		try {
			if(rBindingResult.hasErrors()){//if  validating error
				model.addAttribute("employees", employeeRepository.findAll());
				model.addAttribute("departments", departementRepository.findAll());
				model.addAttribute("designations", designationRepository.findAll());
				model.addAttribute("success", "");
				model.addAttribute("error", "");
				return "/employee/list"; //return to the view with errors
			}
			
			if(employee.getDepartment().getId() ==4 && employee.getOtherDepartement()==""){ //other
				model.addAttribute("employees", employeeRepository.findAll());
				model.addAttribute("departments", departementRepository.findAll());
				model.addAttribute("designations", designationRepository.findAll());
				model.addAttribute("success", "");
				model.addAttribute("error", "Department can not be null");
				return "/employee/list"; //return to the view with errors
			}
			
			//get the message to return, depending if add or update
			String returnMessage = employee.getEmployeeNo()==null ? "Employee Added Succefully":"Employee Updated Succefully";
			//save employee			
			employeeRepository.save(employee);
			redirect.addFlashAttribute("success", returnMessage);
		} catch (Exception e) {
			redirect.addFlashAttribute("error", "Oups : Error occured");
		}		
		return "redirect:/";
	}

}
