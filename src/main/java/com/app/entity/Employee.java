package com.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.jpamodelgen.xml.jaxb.PostPersist;
import org.hibernate.validator.constraints.NotBlank;
/**
 * Employee pojo
 * @author ml.diallo
 *
 */
@Entity
@Table(name="Employee")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long employeeNo;
	
	
	
	@NotBlank
	String firstName;
	
	@NotBlank
	String lastName;
	
	
	@NotNull
	@ManyToOne
	Designation designation;
	
	@NotNull
	@ManyToOne
	Department department;
	
	
	String otherDepartement;
	
	
		
	
	
	public Employee() {
		super();
	}
	public Employee(String firstName, String lastName, Designation designation, Department department,
			String otherDepartement) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.designation = designation;
		this.department = department;
		this.otherDepartement = otherDepartement;
	}
	
	
	
	



	
	public Long getEmployeeNo() {
		return employeeNo;
	}
	public void setEmployeeNo(Long employeeNo) {
		this.employeeNo = employeeNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Designation getDesignation() {
		return designation;
	}
	public void setDesignation(Designation designation) {
		this.designation = designation;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public String getOtherDepartement() {
		return otherDepartement;
	}
	public void setOtherDepartement(String otherDepartement) {
		this.otherDepartement = otherDepartement;
	}
	
}
