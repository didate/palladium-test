package com.app.utils;

import java.util.Arrays;
import java.util.List;

import com.app.entity.Department;
import com.app.entity.Designation;



public class StaticData {

	public static final List<Designation> DESIGNATION = Arrays.asList(
			new Designation(1,"Physician"),
			new Designation(2,"Nurse"), 
			new Designation(3,"Midwife"),new Designation(4,"Lab Tech"), new Designation(5,"Pharmacist"));
	
	public static final List<Department> DEPARTMENT = Arrays.asList(
			new Department(1,"VCT"),
			new Department(2,"CCC"), 
			new Department(3,"OPD"),new Department(4,"Other") );
}
